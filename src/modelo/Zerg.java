package modelo;

import excepciones.ExceptionBatalla;

/**
 *
 * @author Julio
 */
public class Zerg extends Especie {

    private int esbirros;
    private int overlords;

    public Zerg(String nombre, int numerovictorias, int nivelataque, int niveldefensa, int esbirros, int overlods) throws ExceptionBatalla {
        super(nombre, numerovictorias, nivelataque, niveldefensa);
        this.esbirros = esbirros;
        this.overlords = overlods;
    }

    public int getEsbirros() {
        return esbirros;
    }

    public void setEsbirros(int esbirros) {
        this.esbirros = esbirros;
    }

    public int getOverlords() {
        return overlords;
    }

    public void setOverlords(int overlords) {
        this.overlords = overlords;
    }

    @Override
    public double aumentoAtaque() {
        double aumentoAta = getNivelataque() + (esbirros * (0.15));
        double aumentoAtaOver = getNivelataque() + (overlords * (0.40));
        return (getNivelataque() + (aumentoAta + aumentoAtaOver));
    }

    @Override
    public double aumentoDefensa() {
       return getNiveldefensa() + (esbirros * (0.30));
    }

}
