package modelo;

import excepciones.ExceptionBatalla;

/**
 *
 * @author Julio
 */
public class Terran extends Especie {

    private int edificio;
    private int arma;

    public Terran(String nombre, int numerovictorias, int nivelataque, int niveldefensa, int edificio, int arma) throws ExceptionBatalla {
        super(nombre, numerovictorias, nivelataque, niveldefensa);
        this.edificio = edificio;
        this.arma = arma;
    }

    public int getEdificio() {
        return edificio;
    }

    public void setEdificio(int edificio) {
        this.edificio = edificio;
    }

    public int getArma() {
        return arma;
    }

    public void setArma(int arma) {
        this.arma = arma;
    }

    @Override
    public double aumentoAtaque() {
        return getNivelataque()+(arma * (0.50));

    }

    @Override
    public double aumentoDefensa() {
       return getNiveldefensa()+(edificio * (0.25));
    }
}
