package modelo;

import excepciones.ExceptionBatalla;

/**
 *
 * @author Julio
 */
public abstract class Especie {

    private String nombre;
    private int numerovictorias;
    private int nivelataque;
    private int niveldefensa;

    public Especie(String nombre, int numerovictorias, int nivelataque, int niveldefensa) throws ExceptionBatalla {
        if (nombre.equals("")) {
            throw new ExceptionBatalla("ERROR: No puedes dejar en blanco el nombre");
        } else {
            this.nombre = nombre;
        }

        this.numerovictorias = 0;

        if (nivelataque < 1) {
            throw new ExceptionBatalla("ERROR: No puede ser un numero menor a 1");
        } else {
            this.nivelataque = nivelataque;
        }
        if (niveldefensa < 1) {
            throw new ExceptionBatalla("ERROR: No puede ser un numero menor a 1");
        } else {
            this.niveldefensa = niveldefensa;
        }
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) throws ExceptionBatalla {
        if (nombre.equals("")) {
            throw new ExceptionBatalla("ERROR: No puedes dejar en blanco el nombre");
        } else {
            this.nombre = nombre;
        }
    }

    public int getNumerovictorias() {
        return numerovictorias;
    }

    public void setNumerovictorias(int numerovictorias) throws ExceptionBatalla {
        if (numerovictorias < 1) {
            throw new ExceptionBatalla("ERROR: No puede ser un numero menor a 1");
        } else {
            this.numerovictorias = numerovictorias;
        }
    }

    public int getNivelataque() {
        return nivelataque;
    }

    public void setNivelataque(int nivelataque) throws ExceptionBatalla {
        if (nivelataque < 1) {
            throw new ExceptionBatalla("ERROR: No puede ser un numero menor a 1");
        } else {
            this.nivelataque = nivelataque;
        }
    }

    public int getNiveldefensa() {
        return niveldefensa;
    }

    public void setNiveldefensa(int niveldefensa) throws ExceptionBatalla {
        if (niveldefensa < 1) {
            throw new ExceptionBatalla("ERROR: No puede ser un numero menor a 1");
        } else {
            this.niveldefensa = niveldefensa;
        }
    }

    public abstract double aumentoAtaque();

    public abstract double aumentoDefensa();

}
