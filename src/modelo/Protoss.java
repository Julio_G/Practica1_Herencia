package modelo;

import excepciones.ExceptionBatalla;

/**
 *
 * @author Julio
 */
public class Protoss extends Especie {

    private int pilones;

    public Protoss(String nombre, int numerovictorias, int nivelataque, int niveldefensa, int pilones) throws ExceptionBatalla {
        super(nombre, numerovictorias, nivelataque, niveldefensa);
        this.pilones = pilones;
    }
    
    public int getPilones() {
        return pilones;
    }

    public void setPilones(int pilones) {
        this.pilones = pilones;
    }
    
    @Override
    public double aumentoAtaque() {
         return (getNivelataque()+(pilones * 0.5));

    }

    @Override
    public double aumentoDefensa() {
        return (getNiveldefensa()+(pilones * 0.5));  
    }

}
