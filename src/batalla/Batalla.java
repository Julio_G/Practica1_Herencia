package batalla;

import excepciones.ExceptionBatalla;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import modelo.Especie;
import modelo.Protoss;
import modelo.Terran;
import modelo.Zerg;

/**
 *
 * @author Julio
 */
public class Batalla {

    static ArrayList<Especie> escuadron = new ArrayList<>();

    public static void main(String[] args) throws IOException, ExceptionBatalla {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String texto = "";

        do {
            texto = br.readLine();
            String[] dato = texto.split(" ");

            if (dato[0].equalsIgnoreCase("A")) {
                altaEscuadrones(dato);
            }
            if (dato[0].equalsIgnoreCase("R")) {
                añadirBatalla(dato);
            }
            if (dato[0].equalsIgnoreCase("M")) {
                modificarDatos(dato);
            }
            if (dato[0].equalsIgnoreCase("C")) {
                // clasificacionEscuadrones(dato);
            }

        } while (!texto.equalsIgnoreCase("S"));
        System.out.println("Adios");
    }

    public static boolean comprobacionNumero(String str) {
        return (str.matches("[+-]?\\d*(\\.\\d+)?") && str.equals("") == false);
    }

    public static void altaEscuadrones(String[] dato) throws ExceptionBatalla {
        int contInt = 0;
        int contErr = 0;
        int e = 0;

        if (dato.length < 5) {
            System.out.println("ERROR 001: Nº Argumentos Invalido");
        } else {
            if (dato[1].equalsIgnoreCase("TERRAN")) {
                e++;
                int[] datosInt = new int[dato.length];
                if (dato.length == 7) {

                    int escuaRepetido = 0;
                    for (Especie especie : escuadron) {
                        if (especie.getNombre().equalsIgnoreCase(dato[2])) {
                            escuaRepetido = 1;
                        }
                    }
                    if (escuaRepetido == 1) {
                        System.out.println("Error 007: Especie Repetida");
                    } else {
                        boolean compNumeros = true;
                        for (int i = 3; i < dato.length; i++) {
                            if (comprobacionNumero(dato[i])) {
                            } else {
                                compNumeros = false;
                            }

                        }
                        if (compNumeros == false) {
                            System.out.println("Error 003: Dato Incorrecto");
                        } else {
                            for (int i = 3; i < dato.length; i++) {
                                datosInt[i] = Integer.parseInt(dato[i]);
                            }
                            for (int i = 3; i < datosInt.length; i++) {
                                if (datosInt[i] < 1) {
                                    contInt++;
                                }
                            }
                            if (contInt > 0) {
                                System.out.println("Error 003: Dato Incorrecto");
                                contErr++;
                            }
                            if (contErr == 0) {
                                //Da igual que en el numero de victorias ponga 5 porque en la especie esta predeterminado 0.
                                Terran T = new Terran(dato[2], 5, datosInt[3], datosInt[4], datosInt[5], datosInt[6]);
                                escuadron.add(T);
                                System.out.println("OK: Escuadron registrado");
                            }
                        }
                    }
                } else {
                    System.out.println("Error 001: Nº Argumentos Invalido ");
                }
            }
            if (dato[1].equalsIgnoreCase("ZERG")) {
                e++;
                int[] datosInt = new int[dato.length];
                if (dato.length == 7) {

                    int escuaRepetido = 0;
                    for (Especie especie : escuadron) {
                        if (especie.getNombre().equalsIgnoreCase(dato[2])) {
                            escuaRepetido = 1;
                        }
                    }
                    if (escuaRepetido == 1) {
                        System.out.println("Error 007: Especie Repetida");
                    } else {
                        boolean compNumeros = true;
                        for (int i = 3; i < dato.length; i++) {
                            if (comprobacionNumero(dato[i])) {
                            } else {
                                compNumeros = false;
                            }
                        }
                        if (compNumeros == false) {
                            System.out.println("Error 003: Dato Incorrecto");
                        } else {
                            for (int i = 3; i < dato.length; i++) {
                                datosInt[i] = Integer.parseInt(dato[i]);
                            }
                            for (int i = 3; i < datosInt.length; i++) {
                                if (datosInt[i] < 1) {
                                    contInt++;
                                }
                            }
                            if (contInt > 0) {
                                System.out.println("Error 003: Dato Incorrecto");
                                contErr++;
                            }
                            if (contErr == 0) {
                                //Da igual que en el numero de victorias ponga 5 porque en la especie esta predeterminado 0.
                                Zerg Z = new Zerg(dato[2], 5, datosInt[3], datosInt[4], datosInt[5], datosInt[6]);
                                escuadron.add(Z);
                                System.out.println("OK: Escuadron registrado");
                            }
                        }
                    }
                } else {
                    System.out.println("Error 001: Nº Argumentos Invalido ");
                }
            }
            if (dato[1].equalsIgnoreCase("PROTOSS")) {
                e++;
                int[] datosInt = new int[dato.length];
                if (dato.length == 6) {

                    int escuaRepetido = 0;
                    for (Especie especie : escuadron) {
                        if (especie.getNombre().equalsIgnoreCase(dato[2])) {
                            escuaRepetido = 1;
                        }
                    }
                    if (escuaRepetido == 1) {
                        System.out.println("Error 007: Especie Repetida");
                    } else {
                        boolean compNumeros = true;
                        for (int i = 3; i < dato.length; i++) {
                            if (comprobacionNumero(dato[i])) {
                            } else {
                                compNumeros = false;
                            }

                        }
                        if (compNumeros == false) {
                            System.out.println("Error 003: Dato Incorrecto");
                        } else {
                            for (int i = 3; i < dato.length; i++) {
                                datosInt[i] = Integer.parseInt(dato[i]);
                            }
                            for (int i = 3; i < datosInt.length; i++) {
                                if (datosInt[i] < 1) {
                                    contInt++;
                                }
                            }
                            if (contInt > 0) {
                                System.out.println("Error 003: Dato Incorrecto");
                                contErr++;
                            }
                            if (contErr == 0) {
                                //Da igual que en el numero de victorias ponga 5 porque en la especie esta predeterminado 0.
                                Protoss P = new Protoss(dato[2], 5, datosInt[3], datosInt[4], datosInt[5]);
                                escuadron.add(P);
                                System.out.println("OK: Escuadron registrado");
                            }
                        }
                    }
                } else {
                    System.out.println("Error 001: Nº Argumentos Invalido ");
                }
            }
            if (e == 0) {
                System.out.println("Error 002: Especie Erronea");
            }
        }
    }

    public static boolean escudronRepetido(String nescudron) {
        for (Especie esp : escuadron) {
            if (esp.getNombre().equalsIgnoreCase(nescudron)) {
                return true;
            }
        }
        return false;
    }

    public static void añadirBatalla(String[] dato) throws ExceptionBatalla {
        if (dato.length == 3) {

            if (escuadron.size() >= 1) {

                if (dato[1].equalsIgnoreCase(dato[2])) {
                    System.out.println("Error 008: No puede pelear un escuadron contra si mismo");
                } else {
                    int escuaRepetido1 = 0;
                    int escuaRepetido2 = 0;
                    for (Especie especie : escuadron) {
                        if (especie.getNombre().equalsIgnoreCase(dato[1])) {
                            escuaRepetido1 = 1;
                        }
                    }
                    for (Especie especie : escuadron) {
                        if (especie.getNombre().equalsIgnoreCase(dato[2])) {
                            escuaRepetido2 = 1;
                        }
                    }
                    if (escuaRepetido1 == 1 && escuaRepetido2 == 1) {
                        String escuadron1 = dato[1];
                        String escuadron2 = dato[2];
                        Especie esc1 = null;
                        Especie esc2 = null;
                        for (Especie esp : escuadron) {
                            if (esp.getNombre().equalsIgnoreCase(escuadron1)) {
                                esc1 = esp;
                            }
                        }
                        for (Especie esp : escuadron) {
                            if (esp.getNombre().equalsIgnoreCase(escuadron2)) {
                                esc2 = esp;
                            }
                        }
                        int contEsc1 = 0;
                        int contEsc2 = 0;

                        for (int batallas = 1; batallas < 6; batallas++) {
                            int numrand = (int) (Math.random() * 9 + 1);
                            int numrand2 = (int) (Math.random() * 9 + 1);

                            double es1 = numrand + esc1.aumentoAtaque() - esc2.aumentoDefensa();
                            double es2 = numrand2 + esc2.aumentoAtaque() - esc1.aumentoDefensa();

                            //Creamos un string en el que guardamos el nombre del escuadron ganador segun los puntos.
                            String ganador = "";
                            if (es1 > es2) {
                                contEsc1++;
                                ganador = escuadron1;
                            }
                            if (es1 < es2) {
                                contEsc2++;
                                ganador = escuadron2;
                            }
                            if (es1 == es2) {
                                ganador = "Empate";
                            }
                            System.out.println("Asalto nº " + batallas);
                            System.out.println("Ataca " + escuadron1 + " - Nº Aleatorio: " + numrand + " - Valor Ataque: " + es1);
                            System.out.println("Ataca " + escuadron2 + " - Nº Aleatorio: " + numrand2 + " - Valor Ataque: " + es2);
                            System.out.println("Ganador Asalto " + ganador);
                            System.out.println("Resultado Actual= " + escuadron1 + ": " + contEsc1 + " - " + escuadron2 + ": " + contEsc2);
                        }
                        if (contEsc1 == contEsc2) {
                            System.out.println("La batalla ha acabado en empate - Resultado: " + escuadron1 + ": " + contEsc1 + " - " + escuadron2 + ": " + contEsc2);
                        }
                        if (contEsc1 > contEsc2) {
                            int vict1 = esc1.getNumerovictorias();
                            esc1.setNumerovictorias(vict1 + 1);
                            System.out.println("Gana la batalla: " + escuadron1 + " en " + contEsc1 + " partidas");
                        }
                        if (contEsc1 < contEsc2) {
                            int vict2 = esc2.getNumerovictorias();
                            esc2.setNumerovictorias(vict2 + 1);
                            System.out.println("Gana la partida: " + escuadron2 + " en " + contEsc2 + " partidas");
                        }

                    } else {
                        System.out.println("Error 005: No existe especie con ese nombre");
                    }
                }
            } else {
                System.out.println("Error 004: Operacion Incorrecta - (Hacen falta 2 escuadrones creados )");
            }

        } else {
            System.out.println("Error 001: Nº Argumentos Invalido");
        }
    }

    public static void modificarDatos(String[] dato) throws ExceptionBatalla {

        if (dato.length == 4) {
            String escua = "";

            int contEsp = 0;
            for (int i = 0; i < escuadron.size(); i++) {
                if (escuadron.get(i).getNombre().equalsIgnoreCase(dato[1])) {
                    escua = escuadron.get(i).getClass().getSimpleName();
                    if (escua.equalsIgnoreCase("TERRAN")) {
                        contEsp++;
                        Terran T = (Terran) escuadron.get(i);
                        if (dato[2].equalsIgnoreCase("EDIFICIOS")) {
                            int numEdif = 0;
                            if (comprobacionNumero(dato[3])) {
                                numEdif = Integer.parseInt(dato[3]);
                                T.setEdificio(numEdif);
                                System.out.println("OK: Propiedad Mejorada");
                            }
                        } else if (dato[2].equalsIgnoreCase("ARMAS")) {
                            int numArma = 0;
                            if (comprobacionNumero(dato[3])) {
                                numArma = Integer.parseInt(dato[3]);
                                T.setArma(numArma);
                                System.out.println("OK: Propiedad Mejorada");
                            }
                        } else {
                            System.out.println("Error 006: Propiedad Incorrecta");
                        }
                    } else if (escua.equalsIgnoreCase("ZERG")) {
                        contEsp++;
                        Zerg Z = (Zerg) escuadron.get(i);
                        if (dato[2].equalsIgnoreCase("ESBIRROS")) {
                            int numEsbi = 0;
                            if (comprobacionNumero(dato[3])) {
                                numEsbi = Integer.parseInt(dato[3]);
                                Z.setEsbirros(numEsbi);
                                System.out.println("OK: Propiedad Mejorada");
                            }
                        } else if (dato[2].equalsIgnoreCase("OVERLORDS")) {
                            int numOver = 0;
                            if (comprobacionNumero(dato[3])) {
                                numOver = Integer.parseInt(dato[3]);
                                Z.setOverlords(numOver);
                                System.out.println("OK: Propiedad Mejorada");
                            }

                        } else {
                            System.out.println("Error 006: Propiedad Incorrecta");
                        }
                    } else if (escua.equalsIgnoreCase("PROTOSS")) {
                        contEsp++;
                        Protoss P = (Protoss) escuadron.get(i);
                        if (dato[2].equalsIgnoreCase("PILONES")) {
                            int numPilo = 0;
                            if (comprobacionNumero(dato[3])) {
                                numPilo = Integer.parseInt(dato[3]);
                                P.setPilones(numPilo);
                                System.out.println("OK: Propiedad Mejorada");
                            }
                        }
                    } else {
                    }
                }
                if (contEsp == 0) {
                    System.out.println("Error 005: No existe especie con ese nombre");
                }

            }
        } else {
            System.out.println("Error 001: Nº Argumentos Invalido");
        }

    }

    public static void clasifiacionEscuadrones(String[] dato) {

    }
}
